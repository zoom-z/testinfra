#тесты для проверки установки нужного ПО


import pytest
import testinfra

def test_OS (host):
    assert host.file("/etc/os-release").contains("Alpine")

def test_nginx (host):
    nginx=host.package("nginx")
    assert nginx.is_installed
    assert nginx.version.startswith("1.16.1")